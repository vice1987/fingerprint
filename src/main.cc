/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "errors/exit_exception.hpp"
#include "modules/aligner_module.hpp"
#include "options/options.hpp"
#include "utils/blocktimer.hpp"
#include "utils/misc.hpp"

#include "common/constants.hpp"

int main(int argc, char *argv[])
{
	try
	{
		dfp::options opt(argc,argv);

		{
			utils::blocktimer timer(__func__);
			dfp::aligner_module fp(opt); 
			fp.exec();
		}

		// print memory statistics
		size_t vm_peak, rss_peak;
		utils::mem_peak(vm_peak,rss_peak);
		std::cerr << "[main] VmPeak: " << utils::human_mem(vm_peak) << "\n";
		std::cerr << "[main] RssPeak: " << utils::human_mem(rss_peak) << "\n";
	}
	catch( dfp::errors::exit_exception &e )
	{
		std::exit(e.code);
	}

	return EXIT_SUCCESS;
}
