/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <string>
#include <exception>

namespace dfp { namespace errors {

	class fasta_reader_exception : public std::exception
	{

	public:

		fasta_reader_exception(const char *comment)
		{
			std::stringstream ss;
			ss << "[fasta_reader_exception] " << comment << std::endl;
			m_comment = ss.str();
		}

	    fasta_reader_exception(const std::string &comment)
	    {
	    	std::stringstream ss;
			ss << "[fasta_reader_exception] " << comment << std::endl;
			m_comment = ss.str();
	    }

		fasta_reader_exception(const std::string *comment)
		{
			std::stringstream ss;
			ss << "[fasta_reader_exception] " << *comment << std::endl;
			m_comment = ss.str();
		}

	    ~fasta_reader_exception() throw() { }

		const char* what() const throw() { return m_comment.c_str(); }


	private:

		std::string m_comment;

	}; // class IncorrectFormat


} } // namespace dfp::errors
