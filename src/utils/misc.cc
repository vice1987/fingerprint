/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "misc.hpp"

#include <cstring>
#include <cstdint>
#include <unistd.h>
#include <sys/resource.h>

#include <ctime>
#include <cmath>
#include <fstream>
#include <sstream>
#include <iomanip>


namespace utils 
{

	void load_filenames(const std::string &input_file, std::vector<std::string> &names)
	{
		std::string line; // read line
		std::ifstream ifs( input_file.c_str() );

		while( ifs.good() )
		{
			getline(ifs,line);
			if( line != "" ) names.push_back(line);
		}

		ifs.close();
	}


	void mem_peak( size_t &vm_peak, size_t &rss_peak )
	{
		vm_peak  = 0;
		rss_peak = 0;

		std::ifstream stat_stream( "/proc/self/status", std::ios_base::in );

		std::string str;
		while( stat_stream >> str )
		{
			if( str == "VmPeak:" )
				stat_stream >> vm_peak;

			if( str == "VmHWM:" )
				stat_stream >> rss_peak;
		}
	}


	std::string human_time( time_t seconds )
	{
		std::stringstream out;

		long d = seconds / 3600 / 24;
		long h = seconds / 3600;
		long m = (seconds % 3600) / 60;
		long s = (seconds % 3600) % 60;

		if( d > 0 ) out << d << "d ";
		if( h > 0 ) out << h << "h ";
		if( m > 0 ) out << m << "m ";
		out << s << "s";

		return out.str();
	}


	std::string human_mem( size_t kbytes )
	{
		static const std::vector<std::string> suff { "KB", "MB", "GB", "TB" };

	    if( kbytes > 0 )
	    {
	    	std::stringstream ss;
			ss << std::fixed << std::setprecision(2);

	    	size_t i = static_cast<size_t>( std::floor(std::log(kbytes)/std::log(1024)) );
	    	i = i <= 3 ? i : 3;
	    	double x = std::pow(1024,i);
	    	double val = kbytes/x;

	    	ss << val << suff[i];
	    	return ss.str();
	    }

	    return std::string("0KB");
	}


} // namespace utils
