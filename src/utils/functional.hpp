/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <thread>
#include <vector>


namespace utils
{

    template< typename InItor, typename OutItor, typename Function >
    OutItor map( InItor first, InItor last, OutItor result, Function f )
    {
        while( first != last )
            *result++ = f(*first++);
        
        return result;
    }


    template< typename InItor, typename OutItor, typename Function >
    OutItor pmap( InItor first, InItor last, OutItor result, Function f, int t = 0, size_t min_elem = 500 )
    {
        size_t elements = std::distance(first,last);
        
        if( elements < min_elem )
            return map( first, last, result, f );
        
        /* worker function */
        auto worker = [&]( size_t from, size_t to ) 
        {
            InItor  l_first = first + from;
            InItor  l_last = first + to;
            OutItor l_result = result + from;
            
            while( l_first != l_last )
                *l_result++ = f(*l_first++); //f(*l_first++,*l_result++);
        };
        
        /* divide workload across processors */
        size_t hw_concurrency{ std::thread::hardware_concurrency() };
        size_t num_threads{ t > 0 ? static_cast<size_t>(t) : (hw_concurrency > 0 ? hw_concurrency : 1) };
        size_t block_size{ elements/num_threads }; 
        size_t from{0}, max{elements};
        
        /* spawn threads */
        std::vector<std::thread> threads(num_threads+1);

        for( size_t i{0}; i < threads.size(); ++i )
        {
            size_t to{ from+block_size <= max ? from+block_size : max };
            threads[i] = std::thread( worker, from, to );
            from += block_size;
        }
        
        /* wait for them */
        for( auto& t : threads )
            t.join();
        
        return result + elements;
    }


    // Removes all consecutive elements from the range [begin, end) for which predicate p returns true 
    // and returns a past-the-end iterator for the new logical end of the range.
    template< class ForwardIt, class BinaryPredicate >
    ForwardIt strict_unique( ForwardIt begin, ForwardIt end, BinaryPredicate p )
    {
        if( begin == end ) return end;
     
        ForwardIt result, first, second;

        result = first = second = begin;
        while( ++second != end ) 
        {
            if( !p(*first,*second) ) 
            {
                if( std::distance(first,second) == 1 ) 
                    *(result++) = std::move(*first);
                
                first = second;
            }
        }

        if( std::distance(first,second) == 1 ) 
            *(result++) = std::move(*first);

        return result;
    }

} // namespace utils

