/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <iostream>
#include <cassert>
#include <vector>
#include <string>

#include "common/constants.hpp"
#include "common/types.hpp"
#include "data_structures/sequence_descriptor.hpp"


namespace dfp {

	class sequence_store
	{

	public:
		
		using sequence_type = dfp::sequence_descriptor<sequence_store>;
		
		using sequence_pool = std::vector<sequence_type>;
		using pool_index    = std::vector<size_t>;
		using store_type    = std::vector<base_t>;
		
		using iterator = sequence_pool::iterator;
		using const_iterator = sequence_pool::const_iterator;
		

	public:

		// constructors
		sequence_store() { }
		sequence_store( const std::string& filename, size_t min_len=0 ) { load( filename, min_len ); } 
	    sequence_store( const std::vector<std::string>& filenames, size_t min_len=0 ) { load( filenames, min_len ); }

	    // return number of pools
		size_t pools() const { return m_pid2i.size(); }

		// return number of all sequences
		size_t sequences() const { return m_sequences.size(); }
	    size_t size() const { return sequences(); }
	    
	    // return raw pointer to the first base in the storage
	    base_t*       data() { return m_storage.data(); }
	    const base_t* data() const { return m_storage.data(); }

	    // return reference to the i-th base in the storage
	    base_t&       data_at( size_t i ){ return m_storage.at(i); }
	    const base_t& data_at( size_t i ) const { return m_storage.at(i); }

		// iterators on sequences
		iterator       begin() { return m_sequences.begin(); }
		const_iterator begin() const { return m_sequences.begin(); }
		iterator       end() { return m_sequences.end(); }
		const_iterator end() const { return m_sequences.end(); }
		
		// iterators on sequence actual data
		//inline StorageType::iterator       data_begin() { return m_storage.begin(); }
		//inline StorageType::const_iterator data_begin() const { return m_storage.begin(); }
		//inline StorageType::iterator       data_end() { return m_storage.end(); }
		//inline StorageType::const_iterator data_end() const { return m_storage.end(); }

		// return sequence reference with global ID gid
		const sequence_type& at( size_t gid ) const { return m_sequences.at(gid); }
		const sequence_type& operator[]( size_t gid ) const { return m_sequences[gid]; }    

		// return (const) sequence reference with given valid pool and sequence IDs
		const sequence_type& at( size_t pid, size_t sid ) const
		{
			assert( pid < m_pid2i.size() and m_pid2i.at(pid) + sid < m_sequences.size() );
			return m_sequences[ m_pid2i[pid] + sid ];
		}

		// return sequence reference with given valid pool and sequence IDs
		sequence_type& at( size_t pid, size_t sid )
		{
			assert( pid < m_pid2i.size() and m_pid2i.at(pid) + sid < m_sequences.size() );
			return m_sequences[ m_pid2i[pid] + sid ];
		}

		// return the global ID, given pool and sequence IDs,
		size_t gid( size_t pid, size_t sid ) const
		{
			assert( pid < m_pid2i.size() and m_pid2i.at(pid) + sid < m_sequences.size() );
			return m_pid2i[pid] + sid;
		}

		// return number of sequences in a pool
		size_t pool_size( size_t pid )
		{
			assert( pid < m_pid2i.size() );
			return pid+1 == m_pid2i.size() ? (m_sequences.size() - m_pid2i[pid]) : (m_pid2i[pid+1] - m_pid2i[pid]);
		}

		// return the global ID of the first sequence in pool pid
		size_t pool_idx( size_t pid )
		{
			assert( pid < m_pid2i.size() );
			return m_pid2i[pid];
		}

		// remove all sequences
		void clear()
		{
			m_pid2i.clear();
			m_sequences.clear();
			m_storage.clear();
		}

		// load pools of sequences provided a vector of multifasta files
	    void load( const std::vector<std::string>& filenames, size_t min_len=0 )
	    {
	    	for( const auto& fn : filenames )
	    		this->load_pool( fn, min_len );
	    }

	    // load pools of sequences provided a multifasta file
	    void load( const std::string& filename, size_t min_len=0 )
	    {
	    	std::vector<std::string> filenames(1,filename);
			this->load( filenames, min_len );
	    }

	    // write all stored sequences to a file
		void write_to_file( const std::string &filename ) const;


	private:
		
		void load_pool( const std::string &filename, size_t min_len=0 );
		void push_sequence( int32_t pid, int32_t sid, const std::string& name, const std::string& seq );


	private:

		pool_index    m_pid2i;
		sequence_pool m_sequences;
		store_type    m_storage;

	}; // class sequence_store

} // namespace dfp
