/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "data_structures/sequence_descriptor.hpp"
#include "data_structures/sequence_store.hpp"

namespace dfp {

	using sequence = typename dfp::sequence_descriptor< dfp::sequence_store >;

} // namespace dfp
