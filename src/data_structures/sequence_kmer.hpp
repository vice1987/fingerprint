/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <iterator>

#include "common/jf_includes.hpp"
#include "common/types.hpp"
#include "data_structures/sequence.hpp"


namespace dfp {

	// Iterator on kmers of a DnaSequence object
	class sequence_kmer : public std::iterator< std::input_iterator_tag, dfp::kmer > 
	{	
		
	private:
		
		const base_t*  m_beg{nullptr};
		const base_t*  m_cur{nullptr};
		const base_t*  m_end{nullptr};
		const bool     m_canonical{false};
		unsigned int   m_filled{0};
		
		dfp::kmer      m_kmer;            // mer
		dfp::kmer      m_rckmer;          // reverse complement mer

	public:
		
		explicit sequence_kmer( bool canonical = false ) :
			m_canonical(canonical)
		{ }

		sequence_kmer( const sequence_kmer& rhs ) = default;
		
		sequence_kmer( sequence_kmer&& rhs ) :
			m_beg(rhs.m_beg), 
			m_cur(rhs.m_cur), 
			m_end(rhs.m_end), 
			m_canonical(rhs.m_canonical), 
			m_filled(rhs.m_filled),
			m_kmer(std::move(rhs.m_kmer)), 
			m_rckmer(std::move(rhs.m_rckmer))
		{ }


		void reset( const base_t* beg, const base_t* end ) 
		{
			m_filled = 0;
			m_beg    = beg;
			m_cur    = beg;
			m_end    = end;

			this->operator++();
		}

		// precondition: this iterator must correspond to a valid k-mer
		size_t pos() const
		{
			assert( m_cur >= m_beg + dfp::kmer::k() );
			return m_cur - m_beg - dfp::kmer::k();
		}
		
		
		// initialization
		sequence_kmer& operator=( const dfp::sequence& seq ) 
		{
			this->reset( seq.data(), seq.data() + seq.length() );
			return *this;
		}
		
		
		bool operator==(const sequence_kmer& rhs) const {  return m_cur == rhs.m_cur; }	
		bool operator!=(const sequence_kmer& rhs) const {  return m_cur != rhs.m_cur; }


		const dfp::kmer& operator*() const  { return ((not m_canonical) or m_kmer < m_rckmer) ? m_kmer : m_rckmer; }
		const dfp::kmer* operator->() const { return &this->operator*(); }

		// pre-increment
		sequence_kmer& operator++() 
		{
			if(m_cur >= m_end) 
			{
				m_cur = m_end = nullptr;
				return *this;
			}
			
			do // find next (unambiguous) kmer
			{
				int code = static_cast<int>(*m_cur++);
				
				if(code < 4)
				{
					m_kmer.shift_left(code);
					if(m_canonical) m_rckmer.shift_right(m_rckmer.complement(code));
				    m_filled = std::min( m_filled+1, dfp::kmer::k() );
				}
				else
				{
					m_filled = 0;
				}
			} 
			while( m_filled < dfp::kmer::k() and m_cur < m_end );


			if( m_cur >= m_end and m_filled < dfp::kmer::k() ) // no (valid) kmer found
			{
				m_cur = m_end = nullptr;
			}
			
			return *this;
		}
		
		// post-increment
		sequence_kmer operator++(int) 
		{
			sequence_kmer out(*this);
			++(*this);
			return out;
		}
	};

} // namespace dfp
