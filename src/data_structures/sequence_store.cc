/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "sequence_store.hpp"

#include <fstream>
#include "io/fasta_reader.hpp"


namespace dfp {

	void 
	sequence_store::write_to_file( const std::string &filename ) const
	{
		std::ofstream ofs(filename);
		
		for( const auto& seq : m_sequences ) 
			ofs << seq << "\n";
		
		ofs.close();
	}


	void
	sequence_store::load_pool( const std::string& filename, size_t min_len )
	{
		static std::string name;
		static std::string sequence;

		m_pid2i.push_back( m_sequences.size() );

		int32_t pid = static_cast<int32_t>(m_pid2i.size()-1);
		int32_t sid = 0;

		dfp::io::fasta_reader reader(filename);
		while( reader.next_sequence(name,sequence) )
		{
			if( sequence.length() >= min_len )
				this->push_sequence( pid, sid++, name, sequence );
		}
	}


	void
	sequence_store::push_sequence( int32_t pid, int32_t sid, const std::string& name, const std::string& seq )
	{
		size_t off = m_storage.size();
		m_storage.resize( off + seq.length() );
		
		for( size_t j{0}; j < seq.length(); ++j )
			m_storage[off+j] = dfp::constants::int2base[seq[j]];
			
		m_sequences.emplace_back( pid, sid, name, this, off, seq.length() );
	}

} // namespace dfp
