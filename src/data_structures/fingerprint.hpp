/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <vector>

#include "common/jf_includes.hpp"

namespace dfp
{

    namespace details
    {
        template< typename K, typename P >
        class fp_se_kmer_t
        {

        public:

            using kmer_type = K;
            using pos_type = P;

            fp_se_kmer_t( kmer_type mer, pos_type pos ) : m_mer(mer), m_pos(pos) { }

            kmer_type& mer() { return m_mer; }
            const kmer_type& mer() const { return m_mer; }

            P pos() const { return m_pos; }
            size_t ins_size() const { return K::k(); }

            kmer_type get_canonical() const { return m_mer.get_canonical(); }

            bool operator<( const fp_se_kmer_t &rhs ) const { return m_pos < rhs.m_pos; }
            bool operator==( const fp_se_kmer_t &rhs ) const { return m_mer == rhs.m_mer; }
            bool operator!=( const fp_se_kmer_t &rhs ) const { return not this->operator==(rhs); }

        private:

            kmer_type m_mer;
            pos_type  m_pos;

        };

        
        template< typename K, typename P >
        class fp_pe_kmer_t
        {

        public:

            using kmer_type = std::pair<K,K>;
            using pos_type  = std::pair<P,P>;

            fp_pe_kmer_t( kmer_type mers, pos_type pp ) : m_pmer(mers), m_ppos(pp) { }

            kmer_type& mer() { return m_pmer; }
            const kmer_type& mer() const { return m_pmer; }
            
            P pos() const { return m_ppos.first; }
            size_t ins_size() const { return m_ppos.second - m_ppos.first + K::k(); }

            kmer_type get_canonical() const 
            {  
                auto rc_pmer = std::make_pair( m_pmer.second.get_reverse_complement(), m_pmer.first.get_reverse_complement() );
                return m_pmer < rc_pmer ? m_pmer : rc_pmer;
            }

            bool operator<( const fp_pe_kmer_t & rhs ) const { return m_ppos < rhs.m_ppos; }
            bool operator==( const fp_pe_kmer_t &rhs ) const { return m_pmer == rhs.m_pmer; }
            bool operator!=( const fp_pe_kmer_t &rhs ) const { return not this->operator==(rhs); }

        private:

            kmer_type m_pmer;
            pos_type  m_ppos;
        };
    }


    // models a fingerprint as a ordered vector of pairs <kmer,pos>
    template< typename T >
    class fingerprint_t
    {

    public:

        using data_type = T;


    public:

        void add( const T& val ) { m_data.push_back(val); }
        
        size_t size() const { return m_data.size(); }

        size_t seqlen( size_t len ) { m_len = len; return m_len; }
        size_t seqlen() const { return m_len; }

        T& at( size_t i ) { return m_data.at(i); }
        const T& at( size_t i ) const { return m_data.at(i); }

        T& operator[]( size_t i ) { return m_data[i]; }
        const T& operator[]( size_t i ) const { return m_data[i]; }  

        T& kmer( size_t i ) { return m_data.at(i); }
        const T& kmer( size_t i ) const { return m_data.at(i); }

        size_t pos( size_t i ) const { return m_data.at(i).pos(); }

        void sort() { std::sort( m_data.begin(), m_data.end() ); }

        void clear() { m_data.clear(); }

        friend std::ostream& operator<<( std::ostream& os, const fingerprint_t<T>& rhs )
        {
            for( const auto& x : rhs.m_data )
                os << "[" << x.pos() << "--" << x.pos()+x.ins_size() << "] ";

            return os;
        }

    private:

        size_t          m_len{0}; // size of the sequence from which the fingerprint has been built
        std::vector<T>  m_data;

    };

    using single_kmer_t = details::fp_se_kmer_t< dfp::kmer, size_t >;
    using paired_kmer_t = details::fp_pe_kmer_t< dfp::kmer, size_t >;

    using fingerprint = fingerprint_t< single_kmer_t >;
    using paired_fingerprint = fingerprint_t< paired_kmer_t >;

    using fpvec = typename std::vector< fingerprint >;
    //using fpvec = typename std::vector< paired_fingerprint >;

} // namespace detfp
