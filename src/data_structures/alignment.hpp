/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <vector>
#include <algorithm>

namespace dfp
{

	class alignment
	{

	public:

		alignment() = default;
		alignment( const alignment & ) = default;
		alignment( alignment && ) = default;

		alignment( size_t qry_id, size_t qry_beg, size_t qry_end, size_t qry_len, 
			       size_t trg_id, size_t trg_beg, size_t trg_end, size_t trg_len,
			       size_t hits, double identity, bool rev ) :

			m_qry_id(qry_id), 
			m_qry_beg(qry_beg), 
			m_qry_end(qry_end),
			m_qry_len(qry_len),

			m_trg_id(trg_id), 
			m_trg_beg(trg_beg), 
			m_trg_end(trg_end),
			m_trg_len(trg_len),

			m_hits(hits), 
			m_identity(identity), 
			m_rev(rev)
		{ }

		alignment& operator=( const alignment & ) = default;
		alignment& operator=( alignment && ) = default;

		size_t qry_id()  const { return m_qry_id; }
		size_t qry_beg() const { return m_qry_beg; }
		size_t qry_end() const { return m_qry_end; }
		size_t qry_len() const { return m_qry_len; }

		size_t trg_id()  const { return m_trg_id; }
		size_t trg_beg() const { return m_trg_beg; }
		size_t trg_end() const { return m_trg_end; }
		size_t trg_len() const { return m_trg_len; }

		size_t hits()     const { return m_hits; }
		double identity() const { return m_identity; }
		bool   rev()      const { return m_rev; }

		size_t left_overhang() const { return std::min( m_qry_beg, m_rev ? m_trg_len-m_trg_end : m_trg_beg ); }
		size_t right_overhang() const { return std::min( m_qry_len-m_qry_end, m_rev ? m_trg_beg : m_trg_len-m_trg_end ); }

		size_t qry_ovl() const { return qry_end() - qry_beg(); }
		size_t trg_ovl() const { return trg_end() - trg_beg(); }

		size_t length() const { return (qry_ovl()+trg_ovl()) / 2; }

	private:

		size_t m_qry_id;
		size_t m_qry_beg;
		size_t m_qry_end;
		size_t m_qry_len;

		size_t m_trg_id;
		size_t m_trg_beg;
		size_t m_trg_end;
		size_t m_trg_len;

		size_t m_hits{0};
		double m_identity{0.0};
		bool   m_rev{false};
	};

	using alignvec = typename std::vector< dfp::alignment >;

} // namespace dfp
