/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  -----------------------------------------------------------------------
 *
 *  This file is a modified version of part of Jellyfish.
 *
 *  Jellyfish is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jellyfish is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jellyfish.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace dfp { namespace io {

	namespace details {

		/****************************************/
		/* wholesequence_stream implementation  */
		/****************************************/

		inline
		wholesequence_stream::wholesequence_stream( dfp::sequence &seq ) : 
			m_seq(seq) 
		{ }

		inline 
		const dfp::sequence& wholesequence_stream::seq() const 
		{ 
			return m_seq; 
		}

	} // namespace details
		

	/***********************************************/
	/* wholesequence_stream_manager implementation */
	/***********************************************/

	inline
	wholesequence_stream_manager::wholesequence_stream_manager( iterator begin, iterator end, int concurrent_sequences ) :
		m_cur(begin), 
		m_end(end),
		m_open_sequences(0),
		m_concurrency(concurrent_sequences)
	{ }


	inline
	wholesequence_stream_manager::stream_ptr_type wholesequence_stream_manager::next()
	{
		jellyfish::locks::pthread::mutex_lock lock(m_mutex);

		stream_ptr_type res;
		this->next_sequence(res);
		return res;
	}
		

	inline
	int wholesequence_stream_manager::concurrency() const 
	{ 
		return m_concurrency; 
	}


	inline
	int wholesequence_stream_manager::nb_streams() const 
	{ 
		return this->concurrency(); 
	}
		
	inline
	void wholesequence_stream_manager::next_sequence( stream_ptr_type& res )
	{
		if( m_open_sequences >= m_concurrency ) 
			return;
		
		if( m_cur != m_end ) 
			res.reset( new details::wholesequence_stream(*m_cur++) );
	}


	/***************************************/
	/* wholesequence_parser implementation */
	/***************************************/

	inline
	wholesequence_parser::wholesequence_parser( iterator begin, iterator end, int threads ) :
		super(4*threads,4*threads),           // max_producers, buffer_size
		m_streams(4*threads),                 // buffer_size
		m_stream_manager(begin,end,4*threads) // sequence range, stream concurrency
	{
		for( uint32_t i = 0; i < m_streams.size(); ++i ) 
		{
			m_streams.init(i);
			get_next_sequence(m_streams[i]);
		}
	}
	
	
	inline 
	bool wholesequence_parser::produce( uint32_t i, dfp::sequence& seq )
	{
		auto& st = m_streams[i];

		if( st.type == streamstatus_type::PROCESS ) // load sequence from stream
		{
			seq = st.stream_ptr->seq(); 
			get_next_sequence(st);
			return false;
		}

		// no more sequences to be processed
		assert( st.type == streamstatus_type::DONE );
		return true;
	}
	

	inline
	void wholesequence_parser::get_next_sequence(stream_type& st) 
	{
		st.stream_ptr.reset();
		st.stream_ptr = m_stream_manager.next();
		st.type = st.stream_ptr ? streamstatus_type::PROCESS : streamstatus_type::DONE;
	}


} } // namespace dfp::io
