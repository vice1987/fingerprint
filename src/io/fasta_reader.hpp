/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <fstream>
#include <sstream>
#include <string>

#include "errors/fasta_reader_exception.hpp"

namespace dfp { namespace io 
{

	class fasta_reader
	{

	public:

		// throws FastaReaderException
	    fasta_reader( const std::string &filename ) { this->open( filename.c_str() ); }

	    const std::string& filename() const { return m_filename; }
	    
	    const std::ifstream& ifs() const { return m_ifs; }
	    
	    void rewind() { m_ifs.seekg(0); }
		
		// throws FastaReaderException
	    void open( const std::string &filename );
		
		void close() { m_filename = ""; m_ifs.close(); }
		
		// throws FastaReaderException
		bool next_sequence( std::string& name, std::string& sequence )
		{
			bool success = read_fasta_id(name);
			if(success) read_fasta_sequence(sequence);
			
			return success;
		}
		
		
	private:
		
		// throws FastaReaderException
		bool read_fasta_id( std::string& name );

		// throws FastaReaderException
		size_t read_fasta_sequence( std::string& seq );
		

	private:
		
		std::string   m_filename;
		std::ifstream m_ifs;

	};

} } // namespace dfp::io
