/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  -----------------------------------------------------------------------
 *
 *  This file is a modified version of part of Jellyfish.
 *
 *  Jellyfish is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jellyfish is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jellyfish.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <iostream>
#include <streambuf>
#include <cstring>
#include <vector>

#include "data_structures/sequence.hpp"
#include "common/jf_includes.hpp"


namespace dfp { namespace io {

	namespace details {

		// Input stream from a Sequence object alike reading it as a FASTA file
		class sequence_istream : public std::istream
		{

		private:

			// Buffer class associated to this stream
			class sequence_streambuf : public std::streambuf
			{

			public:
				
				// precondition: 0 <= seq[i] <= 4 for each i
				explicit sequence_streambuf( dfp::sequence &seq );


			private:
				
				// possibly refill buffer and return next character
				int_type underflow() override;

				// return next character and "increment next pointer"
				int_type uflow() override;

				int_type pbackfail( int_type ch ) override;
				
				std::streamsize showmanyc() override;

				// copy constructor and assignment are disabled
				sequence_streambuf(const sequence_streambuf &) = delete;
				sequence_streambuf& operator= (const sequence_streambuf &) = delete;
				

			private:

				std::vector<char> m_header;

				char* m_hbeg;
				char* m_hcur;
				char* m_hend;

				const base_t* m_beg;
				const base_t* m_cur;
				const base_t* m_end;

			};


		public:
			
			sequence_istream( dfp::sequence &seq );
			~sequence_istream();
			
		private:
			
			sequence_streambuf  m_buf;
			std::streambuf*     m_tmp;
		};

	} // namespace details


	class sequence_stream_manager
	{
		
	public:

		using stream_ptr_type = std::unique_ptr<std::istream>;
		using iterator        = typename std::vector<dfp::sequence>::iterator;


	public:

		sequence_stream_manager( iterator begin, iterator end, int concurrent_sequences = 1 );	
		
		stream_ptr_type next();
		int concurrency() const;
		int nb_streams() const;

		
	protected:
		
		void next_sequence( stream_ptr_type& res );
	  

	private:
		
		iterator m_cur;
		iterator m_end;
		
		int m_open_sequences;
		const int m_concurrency;
		jellyfish::locks::pthread::mutex_recursive m_mutex;
		
	};

} } // namespace dfp::io


#include "sequence_stream.impl.hpp"
