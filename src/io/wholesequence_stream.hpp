/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  -----------------------------------------------------------------------
 *
 *  This file is a modified version of part of Jellyfish.
 *
 *  Jellyfish is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jellyfish is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jellyfish.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <memory>
#include <vector>

#include "data_structures/sequence.hpp"
#include "common/jf_includes.hpp"


namespace dfp { namespace io {

	namespace details {

		class wholesequence_stream
		{
			
		public:
			
			wholesequence_stream( dfp::sequence &seq );
			
			const dfp::sequence& seq() const;
			
		private:
			
			const dfp::sequence& m_seq;
		};

	} // namespace details


	class wholesequence_stream_manager
	{
		
	public:
		
		using stream_ptr_type = std::unique_ptr< details::wholesequence_stream >;
		using iterator        = typename std::vector< dfp::sequence >::iterator;
		
		
	public:
		
		wholesequence_stream_manager( iterator begin, iterator end, int concurrent_sequences = 1 );
		
		stream_ptr_type next();
		
		int concurrency() const;
		int nb_streams() const;
		

	protected:
		
		void next_sequence( stream_ptr_type& res );

		
	private:
		
		iterator m_cur;
		iterator m_end;
		
		int m_open_sequences;
		const int m_concurrency;
		jellyfish::locks::pthread::mutex_recursive m_mutex;
		
	};


	class wholesequence_parser : public jellyfish::cooperative_pool2< wholesequence_parser, dfp::sequence > 
	{
		using super            = jellyfish::cooperative_pool2< wholesequence_parser, dfp::sequence >;
		using stream_ptr_type  = typename wholesequence_stream_manager::stream_ptr_type;
		using iterator         = typename std::vector< dfp::sequence >::iterator;


	public:

		wholesequence_parser( iterator begin, iterator end, int threads = 1 );
		bool produce( uint32_t i, dfp::sequence& seq );


	private:
		
		enum class streamstatus_type : std::uint8_t
		{
			DONE, 
			PROCESS
		};
		
		struct stream_type 
		{
			streamstatus_type  type{streamstatus_type::DONE};
			stream_ptr_type    stream_ptr{nullptr};
		};

		void get_next_sequence(stream_type& st);
		
		jellyfish::cpp_array<stream_type> m_streams;
		wholesequence_stream_manager      m_stream_manager;
		
	}; // wholesequence_parser


} } // namespace dfp::io

#include "wholesequence_stream.impl.hpp"
