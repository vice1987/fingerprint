/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 *  -----------------------------------------------------------------------
 *
 *  This file is a modified version of part of Jellyfish.
 *
 *  Jellyfish is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  Jellyfish is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with Jellyfish.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common/constants.hpp"

namespace dfp { namespace io {

	namespace details {

		/********************************/
		/* sequence_istream definition  */
		/********************************/

		inline
		sequence_istream::sequence_istream( dfp::sequence& seq ) :
			m_buf(seq),
			m_tmp(this->rdbuf(&m_buf))
		{ }


		inline
		sequence_istream::~sequence_istream()
		{
			this->rdbuf(m_tmp);
		}


		/*********************************/
		/* sequence_streambuf definition */
		/*********************************/

		inline
		sequence_istream::sequence_streambuf::sequence_streambuf( dfp::sequence& seq ) :
			m_beg(seq.data()),
			m_cur(seq.data()),
			m_end(seq.data()+seq.length())
		{ 
			m_header.reserve( seq.name().length() + 2 );
			m_header.push_back('>');
			m_header.insert( m_header.end(), seq.name().begin(), seq.name().end() );
			m_header.push_back('\n');

			m_hbeg = m_header.data();
			m_hcur = m_hbeg;
			m_hend = m_hbeg + m_header.size();
		}


		inline
		typename sequence_istream::sequence_streambuf::int_type 
		sequence_istream::sequence_streambuf::underflow()
		{
			using namespace dfp::constants;

			if( m_cur == m_end )
				return traits_type::eof();

			return traits_type::to_int_type( m_hcur == m_hend ? base2char[*m_cur] : *m_hcur );
		}


		inline
		typename sequence_istream::sequence_streambuf::int_type 
		sequence_istream::sequence_streambuf::uflow()
		{
			using namespace dfp::constants;

		    if( m_cur == m_end )
		        return traits_type::eof();

		    return traits_type::to_int_type( m_hcur == m_hend ? base2char[*m_cur++] : *m_hcur++ );
		}


		inline
		typename sequence_istream::sequence_streambuf::int_type 
		sequence_istream::sequence_streambuf::pbackfail( int_type ch )
		{
			using namespace dfp::constants;

			if( m_hcur != m_hend ) // header not processed
			{
				if( m_hcur == m_hend || ( ch != traits_type::eof() and ch != m_hcur[-1] ) )
					return traits_type::eof();

				return traits_type::to_int_type(*--m_hcur);
			}

			// header processed
			
			if( m_cur == m_beg ) // return last header character
				return traits_type::to_int_type(*--m_hcur);

			if( ch != traits_type::eof() && ch != base2char[m_cur[-1]] )
				return traits_type::eof();

			return traits_type::to_int_type(base2char[*--m_cur]);
		}


		inline
		std::streamsize 
		sequence_istream::sequence_streambuf::showmanyc()
		{
			assert( std::less_equal<char *>()(m_hcur,m_hend) );
		    assert( std::less_equal<const base_t *>()(m_cur,m_end) );
		    
		    return (m_hend-m_hcur)+(m_end-m_cur);
		}

	} // namespace details


	/**************************************/
	/* sequence_stream_manager definition */
	/**************************************/

	inline 
	sequence_stream_manager::sequence_stream_manager( iterator begin, iterator end, int concurrent_sequences ) :
		m_cur(begin), 
		m_end(end),
		m_open_sequences(0),
		m_concurrency(concurrent_sequences)
	{ }


	inline
	sequence_stream_manager::stream_ptr_type sequence_stream_manager::next()
	{
		jellyfish::locks::pthread::mutex_lock lock(m_mutex);
		
		stream_ptr_type out;
		this->next_sequence(out);
		return out;
	}


	inline
	int sequence_stream_manager::concurrency() const 
	{ 
		return m_concurrency; 
	}


	inline
	int sequence_stream_manager::nb_streams() const 
	{ 
		return this->concurrency(); 
	}


	inline
	void sequence_stream_manager::next_sequence( stream_ptr_type& isp ) 
	{
		if( m_open_sequences >= m_concurrency )
			return;
		
		if( m_cur != m_end ) 
		{
			isp.reset( new details::sequence_istream(*m_cur++) );
			if( isp->good() ) return;
			
			isp.reset();
			throw std::runtime_error( jellyfish::err::msg() << "Can't open sequence" );
		}
	}


} } // namespace dfp::io
