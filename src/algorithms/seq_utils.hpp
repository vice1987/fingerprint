/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <algorithm>

#include "common/types.hpp"

namespace dfp {

	namespace {

		// Reverse complement of a DNA sequence seq of length len
		void reverse_complement( base_t *seq, size_t len )
		{
			using dfp::constants::base_rc;

			if( len == 0 ) return;

		    size_t p{0};
		    size_t q{len-1};

		    while( p <= q )
		    {
		    	if( p < q )
		    	{
		    		std::swap(seq[p],seq[q]);
		            seq[p] = base_rc[seq[p]];
		            seq[q] = base_rc[seq[q]];
		    	}
		    	else
		    	{
		    		seq[p] = base_rc[seq[p]];
		    	}

		    	++p; --q;
		    }
		}


		// modify an interval [start,end) in a sequence of length len 
		// such that it refers to the reverse complemented sequence
		void reverse_coordinates( size_t &start, size_t &end, size_t len )
		{
		    size_t tmp = start;
		    start = len-end;
		    end = start + (end-tmp);
		}


		// returns the reverse complement of a kmer
		dfp::kmer reverse_complement( const dfp::kmer& kmer )
		{
		    dfp::kmer rc(kmer);
		    rc.reverse_complement();
		    return rc;
		}


		// returns the kmer at a certain position pos in a sequence
		// precondition: kmer at pos is unambiguous
		dfp::kmer kmer_at( base_t *seq, size_t pos )
		{
			dfp::kmer out;
			base_t* cur{seq};
			size_t size{0};

			for( size_t i{0}; i < dfp::kmer::k(); ++i )
			{
				int code = static_cast<int>(*cur++);
				out.shift_left(code);

				if(code < 4) 
					++size;
			}

			assert( size == dfp::kmer::k() );

			return out;
		}


		// Find longest strictly increasing/decreasing subsequence (LIS). 
		// Complexity: O(n log|LIS|).
		
		template< bool SIS, typename T >
		std::vector<size_t> find_lss( const std::vector<T>& a )
		{
		    std::vector<size_t> lss;
		    if(a.empty()) return lss;

		    size_t u,v;
		    std::vector<size_t> p(a.size());
		 
		    lss.emplace_back(0);

		    for( size_t i{1}; i < a.size(); ++i )
		    {
		        // If next element a[i] is greater than last element of
		        // current longest subsequence a[lss.back()], 
		        // just push it at back of the LIS and continue
		        if( SIS ? a[lss.back()] < a[i] : a[lss.back()] > a[i] ) 
		        {
		            p[i] = lss.back();
		            lss.push_back(i);
		            continue;
		        }

		        // Binary search to find the first element referenced by LIS which is strictly bigger than a[i]
		        for( u = 0, v = lss.size()-1; u < v; )
		        {
		            size_t c = (u+v)/2;
		            if( SIS ? a[lss[c]] < a[i] : a[lss[c]] > a[i] ) u = c+1; else v = c;
		        }

		        // Update LIS if new value is smaller than previously referenced value 
		        if( SIS ? a[i] < a[lss[u]] : a[i] > a[lss[u]] )
		        {
		            if(u > 0) p[i] = lss[u-1];
		            lss[u] = i;
		        }
		    }

		    for( u = lss.size(), v = lss.back(); u-- > 0; v = p[v] ) { lss[u] = v; }

		    return lss;
		}


		/*template< bool SEEK_FWD_OLP, typename T >
		std::vector<size_t> find_lss_2( const std::vector<T>& hsp )
		{
			if(hsp.empty()) return std::vector<size_t>();

			using hsp_pos_t = std::pair<size_t,size_t>;

		    std::vector<int> lis( hsp.size() );
		    std::vector<size_t> len( hsp.size() );
		    std::vector<hsp_pos_t> a_reg(hsp.size());
		    std::vector<hsp_pos_t> b_reg(hsp.size());
		    
		    lis[0] = 0;
		    len[0] = 1;
		    a_reg[0] = { hsp[0].first.pos(), hsp[0].first.pos()+hsp[0].first.ins_size() };
		    b_reg[0] = { hsp[0].second.pos(), hsp[0].second.pos()+hsp[0].second.ins_size() };

		    for( size_t i{1}; i < hsp.size(); ++i )
		    {
		    	// find best lis which can be extended with gap constraint
		    	double best_score{1.0};
		    	size_t best_idx{hsp.size()};
		    	bool   found{false};

		    	size_t qry_beg = hsp[i].first.pos(); size_t qry_end = qry_beg + hsp[i].first.ins_size();
			    size_t trg_beg = hsp[i].second.pos(); size_t trg_end = trg_beg + hsp[i].second.ins_size();

		    	// check whether LIS[j] can be extended with a[i], for each j<i
		    	for( ssize_t j = i; --j >= 0; )
		    	{
		    		bool included   = qry_end <= a_reg[j].second and (SEEK_FWD_OLP ? trg_end <= b_reg[j].second : b_reg[j].first <= trg_beg);
			        
			        double qry_diff = static_cast<double>(a_reg[j].second - qry_beg) / (qry_end - a_reg[j].first);
			        double trg_diff = SEEK_FWD_OLP ?
			                          static_cast<double>(b_reg[j].second - trg_beg) / (trg_end - b_reg[j].first):
			                          static_cast<double>(trg_end - b_reg[j].first) / (b_reg[j].second - trg_beg);
			        double rel_diff = (qry_diff >= trg_diff) ? (qry_diff - trg_diff) : (trg_diff - qry_diff);

			        if( !included and rel_diff <= 0.05f and rel_diff < best_score )
			        {
			        	best_score = rel_diff;
			        	best_idx   = j;
			        	found      = true;
			        }
		    	}

		    	lis[i] = found ? best_idx : i;
		    	len[i] = 1 + (found ? len[best_idx] : 0);
		    	a_reg[i] = { found ? std::min(a_reg[i].first,qry_beg) : qry_beg, found ? std::max(a_reg[i].second,qry_end) : qry_end };
		    	b_reg[i] = { found ? std::min(b_reg[i].first,trg_beg) : trg_beg, found ? std::max(b_reg[i].second,trg_end) : trg_end };
		    }

		    size_t best_idx{lis.size()};
		    size_t best_len{0};
		    double best_score{0.0};
		    
		    // find best LIS
		    for( size_t i{0}; i < lis.size(); ++i )
		    {
		    	double min_reg = std::min( a_reg[i].second-a_reg[i].first, b_reg[i].second-b_reg[i].first );
		    	double max_reg = std::max( a_reg[i].second-a_reg[i].first, b_reg[i].second-b_reg[i].first );
		    	
		    	if( len[i] > best_len or (len[i]==best_len and min_reg/max_reg > best_score) )
		    	{
		    		best_idx   = i;
		    		best_len   = len[i];
		    		best_score = min_reg/max_reg;
		    	}
		    }

		    std::vector<size_t> out( best_len );
		    for( size_t z{best_idx}; lis[z] != z; z = lis[z] )
		    	out.at( len[z]-1 ) = z;

		    return out;
		}*/
		

	} // namespace $$$

} // namespace dfp
