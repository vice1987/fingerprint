/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "kmer_count.hpp"

#include <thread>
#include <vector>

#include "io/sequence_stream.hpp"
#include "utils/blocktimer.hpp"

namespace dfp
{

	void kmer_count( dfp::kmerhashtable& kht, dfp::sequence_store& seqstore, const dfp::options& opt, bool canonical )
	{
		utils::blocktimer timer(__func__);

	    using sequence_parser = typename jellyfish::mer_overlap_sequence_parser< dfp::io::sequence_stream_manager >;
	    using kmer_iterator   = typename jellyfish::mer_iterator<sequence_parser,jellyfish::mer_dna>;

	    std::vector<std::thread> threads( opt.nthreads );

	    dfp::io::sequence_stream_manager ssm( seqstore.begin(), seqstore.end(), 4 * opt.nthreads );
	    sequence_parser parser( dfp::kmer::k(), ssm.nb_streams(), 4 * opt.nthreads, 4096, ssm );

	    auto worker = [&](void)
	    {
	        kmer_iterator mers{ parser, canonical };
	            
	        for( ; mers; ++mers) 
	            kht.add(*mers,1);
	        
	        kht.done();
	    };

	    for( auto& t : threads ) 
	        t = std::thread(worker);

	    for( auto& t : threads )
	        t.join();
	}

} // namespace dfp
