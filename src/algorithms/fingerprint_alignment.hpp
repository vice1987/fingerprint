/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "common/jf_includes.hpp"
#include "data_structures/alignment.hpp"
#include "data_structures/fingerprint.hpp"
#include "data_structures/sequence.hpp"
#include "options/options.hpp"


namespace dfp {
	
	namespace details {

		/*************************************/
		/* auxiliary data structures / types */
		/*************************************/
		
		// models the belonging of a kmer to a fingerprint (provided with its global id + index)
		template< typename KT >
	    class kmerrec_t
	    {
	    	using kmer_type = typename KT::kmer_type;

	    public:

	        kmerrec_t( const kmer_type& mer, size_t f, size_t i );

	        const kmer_type& kmer() const;
	        size_t gid() const;
	        size_t idx() const;

	        bool operator<( const kmerrec_t& ) const;

	    private:

	    	kmer_type m_kmer;
	        size_t    m_gid;  // sequence/fingerprint global id
	        size_t    m_idx;  // fingerprint index of the kmer

	    };

	    using kmerrec = kmerrec_t< single_kmer_t >;
	    //using kmerrec = kmerrec_t< paired_kmer_t >;


	    // models a shared kmer between two fingerprints
	    class kmerhit
	    {

	    public:

	        kmerhit( size_t qry_gid, size_t trg_gid, size_t qry_idx, size_t trg_idx );

	        size_t qry_gid() const;
	        size_t qry_idx() const;

	        size_t trg_gid() const;
	        size_t trg_idx() const;

	        bool operator<( const kmerhit& rhs ) const;

	    private:

	    	size_t m_qry_gid;  // global id of query sequence
	        size_t m_qry_idx;  // index of hit in query's fingerprint

	        size_t m_trg_gid;  // global id of target sequence
	        size_t m_trg_idx;  // index of hit in target's fingerprint

	    };


	    // models a list of shared hits between two fingerprints
	    class kmerchain
	    {

	    public:

	        kmerchain( size_t qry_gid, size_t trg_gid, size_t hits, bool rev );
	        kmerchain( size_t qry_gid, size_t trg_gid, size_t hits, bool rev, size_t qry_idx, size_t trg_idx );

	        size_t qry_gid() const;
	        size_t trg_gid() const;
	        size_t hits()    const;
	        bool   rev()     const;

	        const std::vector<size_t>& qry_chain() const;
	        const std::vector<size_t>& trg_chain() const;

	        size_t qry_first() const;
	        size_t trg_first() const;

	        size_t qry_last() const;
	        size_t trg_last() const;

	        size_t size() const;

	        void add( size_t qry_idx, size_t trg_idx );

	    private:

	        size_t m_qry_gid;
	        size_t m_trg_gid;
	        size_t m_hits;
	        bool   m_rev{false};

	        std::vector< size_t > m_qry_chain; // indices of kmers in fingerprint a_gid
	        std::vector< size_t > m_trg_chain; // indices of kmers in fingerprint b_gid
	    };


	    using kmervec  = typename std::vector< kmerrec >;
	    using hitvec   = typename std::vector< kmerhit >;
	    using chainvec = typename std::vector< kmerchain >;


	    /***********************/
	    /* auxiliary functions */
	    /***********************/	

	    kmervec 
		kmer_table_build( const fpvec& fingerprints, bool filter_dup );

		hitvec
		hit_table_build( const sequence_store& seqstore, const kmervec& kmertable, bool self_align );

		chainvec
	    cluster_hits( const fpvec& fingerprints, const hitvec& hit_table, const options& opt );

	    void 
	    cluster_lss_thr( const fpvec& fingerprints, const hitvec& hit_table, size_t start, size_t end, chainvec& chains, const dfp::options& opt );

	    void 
	    cluster_hits_thr( const fpvec& fingerprints, const hitvec& hit_table, size_t start, size_t end, chainvec& chains, const options& opt );

	    alignment 
	    align_chain( const kmerchain& chn, const sequence_store& seqstore, const fpvec& fingerprints, const options& opt );

	    template< bool >
	    void 
	    seek_chains( const fpvec& fingerprints, size_t qry_id, size_t trg_id,
	    	         std::vector<size_t>& qry_hits, std::vector<size_t>& trg_hits,
	    	         chainvec& chains, 
	    	         const dfp::options& opt );

	    template< bool, typename TP >
	    bool
	    test_and_extend( kmerchain &chn, const TP& qry_fp, const TP& trg_fp, size_t qry_idx, size_t trg_idx );

	    template< bool >
	    void 
	    seek_chains_2( const fpvec& fingerprints, size_t qry_id, size_t trg_id,
	    	         std::vector<size_t>& qry_hits, std::vector<size_t>& trg_hits,
	    	         chainvec& chains, 
	    	         const dfp::options& opt );

	    template< bool, typename TP >
	    bool
	    try_extension( const kmerchain &chn, const TP& qry_fp, const TP& trg_fp, size_t qry_idx, size_t trg_idx, double& score );

	} // namespace details

	
	alignvec 
	fingerprint_alignment( const sequence_store& seqstore, const fpvec& fingerprints, const options& opt );


} // namespace detfp

#include "fingerprint_alignment.impl.hpp"
