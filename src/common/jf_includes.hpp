/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

// disable following warnings for jellyfish header
#if defined(__clang__)
#	pragma clang diagnostic push
#	pragma clang diagnostic ignored "-Wshorten-64-to-32"
#elif (defined(__GNUC__) || defined(__GNUG__))
#	pragma GCC diagnostic push
#	pragma GCC diagnostic ignored "-Wpedantic"
#	pragma GCC diagnostic ignored "-Wunused-parameter"
#endif

// include jellyfish headers
#include <jellyfish/mer_dna.hpp>
#include <jellyfish/hash_counter.hpp>
#include <jellyfish/mer_overlap_sequence_parser.hpp>
#include <jellyfish/mer_iterator.hpp>

// re-enable warnings
#if defined(__clang__)
#	pragma clang diagnostic pop
#elif (defined(__GNUC__) || defined(__GNUG__))
#	pragma GCC diagnostic pop
#endif


namespace dfp
{

	using kmer = jellyfish::mer_dna;
	using kmerhashtable = jellyfish::cooperative::hash_counter<kmer>;

} // namespace dfp
