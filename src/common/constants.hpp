/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include <vector>
#include <string>

#include "common/types.hpp"

namespace dfp { namespace constants {
	
	// the following const objects have internal linkage
	// anonymous namespace just used for safety
	namespace { 

		// minimum k-mer length
		const uint32_t MIN_KMER = 5;

		// maximum k-mer length
		const uint32_t MAX_KMER = 31;

		// line length in output for fasta sequences
		const uint32_t DEFAULT_FASTA_COLUMNS = 60;

		// WARNING: do not change the following constants!
		const base_t BASE_A = 0;
		const base_t BASE_C = 1;
		const base_t BASE_G = 2;
		const base_t BASE_T = 3;
		const base_t BASE_N = 4;

		// base_rc[BASE_X] is the reverse complement of BASE_X,
		// where X is in {A,C,T,G,N}
		const base_t base_rc[5] = { 3, 2, 1, 0, 4 };

		// converts BASE_X to char, where X is in {A,C,T,G,N}
		const char * const base2char = "ACGTN";

		// lookup table for int to base_t conversion,
		// BASE_N is assigned for unknown bases
		const base_t int2base[256] =
		{
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 0, 4, 1, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 0, 4, 1, 4, 4, 4, 2, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 3, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4,
			4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4, 4
		};

		// Smith-Waterman score constants
		const int8_t MATCH    =   1; // match score = 1
		const int8_t MISMATCH =   4; // mismatch penalty = 4
		const int8_t AMBMATCH =  -1; // unknown base match score = 0
		const int8_t GAP_OPEN =   6; // gap open penalty = 6
		const int8_t GAP_EXT  =   1; // gap extension penalty = 1

		// Smith-Waterman score matrix
		const int8_t SW_MAT[25] = 
		{
			    MATCH, -MISMATCH, -MISMATCH, -MISMATCH, AMBMATCH,
			-MISMATCH,     MATCH, -MISMATCH, -MISMATCH, AMBMATCH,
			-MISMATCH, -MISMATCH,     MATCH, -MISMATCH, AMBMATCH,
			-MISMATCH, -MISMATCH, -MISMATCH,     MATCH, AMBMATCH,
			 AMBMATCH,  AMBMATCH,  AMBMATCH,  AMBMATCH, AMBMATCH
		};

	} // namespace

} } // namespace dfpo::constants

