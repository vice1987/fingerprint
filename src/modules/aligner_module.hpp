/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#pragma once

#include "data_structures/alignment.hpp"
#include "data_structures/sequence.hpp"
#include "data_structures/fingerprint.hpp"
#include "options/options.hpp"

namespace dfp
{

	class aligner_module
	{

	public:

		aligner_module() = default;
		aligner_module( const dfp::options& opt ) : m_opt(opt) { }

		void exec();

	private:

		void check_input_files( const std::vector<std::string>& files ) const;
		dfp::fpvec build_fingerprints( dfp::sequence_store& seqstore );
		void print_fingerprints_stats( const dfp::fpvec& fingerprints ) const;
		dfp::alignvec align_fingerprints( const dfp::sequence_store&, const dfp::fpvec& fingerprints );

	private:

		const dfp::options m_opt{};

	}; // end of class ModuleFilter

} // namespace dfp
