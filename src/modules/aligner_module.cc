/*
 *  This file is part of DFP-OVERLAP
 *  Copyright (c) by 
 *  Riccardo Vicedomini <rvicedomini@appliedgenomics.org>
 *
 *  DFP-OVERLAP is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  DFP-OVERLAP is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with DFP-OVERLAP.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "aligner_module.hpp"

#include <iostream>
#include <fstream>
#include <cassert>
#include <map>

#include <boost/filesystem.hpp>
namespace boost_fs = boost::filesystem;

#include "algorithms/fingerprint_alignment.hpp"
#include "algorithms/fingerprint_build.hpp"
#include "algorithms/kmer_count.hpp"
#include "common/algorithms.hpp"
#include "errors/exit_exception.hpp"
#include "io/sequence_stream.hpp"

#include "utils/blocktimer.hpp"
#include "utils/functional.hpp"
#include "utils/misc.hpp"


namespace dfp
{

	void aligner_module::exec()
	{
		// Set length of mers
		dfp::kmer::k(m_opt.k);

		// load fasta filenames
		std::vector< std::string > fasta_files;
		this->check_input_files( {m_opt.input_fasta} );
	    utils::load_filenames(m_opt.input_fasta,fasta_files);

	    // check input files existence
	    std::cout << "[main] checking input files." << std::endl;
	    this->check_input_files( fasta_files );

		// load sequence pools and allocate fingerprints vector
	    std::cout << "[main] loading sequences." << std::endl;
	    dfp::sequence_store seqstore;
	    seqstore.load(fasta_files);

		// count k-mers and build fingerprints
		auto fingerprints = this->build_fingerprints(seqstore);

		// print fingerprint statistics
		this->print_fingerprints_stats(fingerprints);

		// align fingerprints
		auto alignments = this->align_fingerprints(seqstore,fingerprints);

		// write alignments in output

		std::cout << "[main] writing alignments in output." << std::endl;
		
		std::string olp_file = m_opt.out_prefix + ".olp";
		std::ofstream ofs( olp_file );
		for( const auto& aln : alignments )
		{
			// size_t qry_loh = aln.qry_beg();
			// size_t trg_loh = aln.rev() ? trg.length() - aln.trg_end() : aln.trg_beg();
			// size_t qry_roh = qry.length() - aln.qry_end();
			// size_t trg_roh = aln.rev() ? aln.trg_beg() : trg.length() - aln.trg_end();

			const dfp::sequence& qry = seqstore.at(aln.qry_id());
			const dfp::sequence& trg = seqstore.at(aln.trg_id());

			ofs << qry.name() << ";" << trg.name() 
				<< "\t" << aln.hits() << "\t" << aln.identity()
			    << "\t" << aln.qry_ovl() << "\t" << aln.trg_ovl()
			    << "\t" << aln.qry_beg() << "\t" << aln.qry_end() << "\t" << aln.qry_len()
			    << "\t" << aln.trg_beg() << "\t" << aln.trg_end() << "\t" << aln.trg_len()
			    //<< "\t" << (qry_loh <= trg_loh ? "qry_left" : "trg_left") 
			    << "\t" << aln.left_overhang()
			    //<< "\t" << (qry_roh <= trg_roh ? "qry_right" : "trg_right") 
			    << "\t" << aln.right_overhang() 
			    << "\t" << (aln.rev() ? '-' : '+') 
			    << "\tOLP" << "\n";
		}
		ofs.close();

		olp_file = m_opt.out_prefix + ".olp.m4";
		ofs.open( olp_file );
		for( const auto& aln : alignments )
		{
			const dfp::sequence& qry = seqstore.at(aln.qry_id());
			const dfp::sequence& trg = seqstore.at(aln.trg_id());

			ofs << qry.name() 
				<< "\t" << trg.name() 
				<< "\t" << aln.hits() 
				<< "\t" << aln.identity()
			    << "\t" << "0" 
			    << "\t" << aln.qry_beg() 
			    << "\t" << aln.qry_end() 
			    << "\t" << aln.qry_len()
			    << "\t" << (aln.rev() ? "1" : "0") 
			    << "\t" << aln.trg_beg() 
			    << "\t" << aln.trg_end() 
			    << "\t" << aln.trg_len()
			    //<< "\t0\t0"
			    << "\n";
		}
		ofs.close();
	}


	void aligner_module::check_input_files( const std::vector<std::string>& files ) const
	{
	    // check existence of each provided fasta file
	    for( const auto& fn : files )
	    {
	        if( (not boost_fs::exists(fn)) or (not boost_fs::is_regular_file(fn)) )
	        {
	            std::cerr << "[error] file \"" << fn << "\" does not exist." << std::endl;
	            throw dfp::errors::exit_exception(2);
	        }
	    }
	}


	// TODO: make seqstore a const dfp::sequence_store reference type
	dfp::fpvec aligner_module::build_fingerprints( dfp::sequence_store& seqstore )
	{
		dfp::fpvec fingerprints(seqstore.size());

		// set jellyfish hash table constants
		const uint32_t num_reprobes {126}; // 126 = default jellyfish value
		const uint32_t counter_len  {7};   //   7 = default jellyfish value

		// create jellyfish hash table
		std::cout << "[main] creating hash table for k=" << dfp::kmer::k() << "." << std::endl;
		dfp::kmerhashtable kmer_ht( m_opt.genome_size, dfp::kmer::k()*2, counter_len, m_opt.nthreads, num_reprobes );

		// count k-mers
		std::cout << "[main] counting k-mers." << std::endl;
		dfp::kmer_count( kmer_ht, seqstore, m_opt );

		// build fingerprints
		std::cout << "[main] building fingerprints." << std::endl;
		dfp::fingerprint_build( kmer_ht, seqstore, fingerprints, m_opt );

		return fingerprints;
	}


	void aligner_module::print_fingerprints_stats( const dfp::fpvec& fingerprints ) const
	{
		size_t num_fp{0};
		size_t num_kmers{0};
		size_t total_kmers{0};
		size_t total_seqlen{0};
		size_t total_fplen{0};

		std::cout << "[main] computing fingerprint statistics." << std::endl;

		for( const auto& fp : fingerprints )
		{
			if( fp.size() > 0 )
			{
				num_fp++;
				num_kmers    += fp.size();
				total_kmers  += fp.seqlen() >= dfp::kmer::k() ? fp.seqlen()-dfp::kmer::k()+1 : 0; 
				total_seqlen += fp.seqlen();
				total_fplen  += fp.size() * dfp::kmer::k();
			}
		}

		std::cout << "     * computed fingerprints: " << num_fp << "(of " << fingerprints.size() << ")" << std::endl;
		std::cout << "     * average kmers per fingerprint: " << (num_fp > 0 ? num_kmers/num_fp : 0.0) << std::endl;
		std::cout << "     * fingerprint-sequence length ratio: " << (total_seqlen > 0 ? static_cast<double>(total_fplen)/total_seqlen : 0.0) << std::endl;
		std::cout << "     * fingerprint-sequence kmer ratio: " << (total_kmers > 0 ? static_cast<double>(num_kmers)/total_kmers : 0.0) << std::endl;
	}


	dfp::alignvec aligner_module::align_fingerprints( const dfp::sequence_store& seqstore, const dfp::fpvec& fingerprints )
	{
		// align fingerprints
		std::cout << "[main] aligning sequences." << m_opt.min_identity << std::endl;
		//auto alignments = align_fingerprints(chains,seqstore,fingerprints,m_opt);
		
		auto alignments = dfp::fingerprint_alignment( seqstore, fingerprints, m_opt );
		
		// remove alignments with same id and with low identity

		auto is_bad = [&]( const alignment& aln ){ 
			return (aln.hits() == 0)                                       // hits = 0 refers to overlaps which are not good enough
				or (aln.qry_id() == aln.trg_id())                          // "self" overlap
				or ((m_opt.use_sw and !m_opt.fast_mode) and aln.identity() < m_opt.min_identity)  // low identity (check only if SW is enabled)
				or ((m_opt.use_sw or m_opt.fast_mode) and aln.length() < m_opt.min_overlap);    // short overlap (check only if SW is enabled)
		};

		std::cout << "[main] filtering alignments with min identity = " << m_opt.min_identity << std::endl;
		alignments.erase( std::remove_if( alignments.begin(), alignments.end(), is_bad ), alignments.end() );

		return alignments;
	}


} // namespace dfp
